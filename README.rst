image_tools
========

Python tools for image manipulation

Features
--------
- Open image
- Scale, resize, flip and rotate image
- Convert type
- Adjustion RGB
- Control brightness 

Usage
-----

.. code-block:: bash

    $ pip install imtool

Documentation
-------------

Schedule's documentation lives at `imtool.readthedocs.io <https://imtool.readthedocs.io/>`_.

Please also check the FAQ there with common questions.


Meta
----

Nguyen Khac Thanh - nguyenkhacthanh244@gmail.com

Distributed under the MIT license. See `LICENSE.txt <https://github.com/thanhngk/imtool/blob/master/LICENSE.txt>`_ for more information.

https://github.com/thanhngk/imtool
