"""Command line tool
"""

import os
from functools import wraps
from PIL import Image, ImageEnhance
import click


def get_ext(filename):
    """Get extension of filename
    """
    basename = os.path.basename(filename)
    return basename.split(".")[-1].upper()


def remove_filename(func):
    """Fix pass parameters error
    """
    @wraps(func)
    def decorator(*args, **kwargs):
        if "src" in kwargs:
            kwargs.pop("src")
        if "dest" in kwargs:
            kwargs.pop("dest")
        return func(*args, **kwargs)
    return decorator


def fopen(func):
    """Decorator open image file
    """
    @wraps(func)
    def decorator(*args, **kwargs):
        if "src" not in kwargs:
            return None
        try:
            image_obj = Image.open(kwargs["src"])
        except IOError as err:
            click.echo(str(err))
        else:
            kwargs.update({"image_obj": image_obj})
            return func(*args, **kwargs)
    return decorator


def fsave(func):
    """Decorator save image file
    """
    @wraps(func)
    def decorator(*args, **kwargs):
        result = func(*args, **kwargs)
        if "dest" not in kwargs or not kwargs.get("dest"):
            kwargs["dest"] = kwargs["src"]
        ext = get_ext(kwargs["dest"])
        result.save(kwargs["dest"], ext)
    return decorator


@click.group()
def main():
    """Simple program for manipulation image
    """
    return None


@click.command()
@click.option("-i", "--src", required=True, type=click.STRING)
@click.option("-o", "--dest", default="", type=click.STRING)
@click.option("-w", "--width", default=0, type=click.IntRange(min=0))
@click.option("-h", "--height", default=0, type=click.IntRange(min=0))
@fsave
@fopen
@fopen
@remove_filename
def resize(width, height, image_obj):
    """Resize image
    """
    size = (width, height)
    image_obj.thumbnail(size, Image.ANTIALIAS)
    return image_obj


@click.command()
@click.option("-i", "--src", required=True, type=click.STRING)
@click.option("-o", "--dest", default="", type=click.STRING)
@click.option("-d", "--degree", default=0, type=click.IntRange(min=0, max=360))
@fsave
@fopen
@fopen
@remove_filename
def rotate(degree, image_obj):
    """Rotate image
    """
    return image_obj.rotate(degree)


@click.command()
@click.option("-i", "src", required=True, type=click.STRING)
@fopen
@remove_filename
def show(image_obj):
    """Display image to screen
    """
    image_obj.show()


@click.command()
@click.option("-i", "--src", required=True, type=click.STRING)
@click.option("-o", "--dest", default="", type=click.STRING)
@click.option("-r", "--red", default=0, type=click.IntRange(min=0, max=255))
@click.option("-g", "--green", default=0, type=click.IntRange(min=0, max=255))
@click.option("-b", "--blue", default=0, type=click.IntRange(min=0, max=255))
@fsave
@fopen
@fopen
@remove_filename
def balance(red, green, blue, image_obj):
    """Adjustion balance image
    """
    for p_x in range(image_obj.width):
        for p_y in range(image_obj.height):
            pixel = list(image_obj.getpixel((p_x, p_y)))
            pixel[0] = min(pixel[0] + red, 255)
            pixel[1] = min(pixel[1] + green, 255)
            pixel[2] = min(pixel[2] + blue, 255)
            image_obj.putpixel((p_x, p_y), tuple(pixel))
    return image_obj


@click.command()
@click.option("-i", "--src", required=True, type=click.STRING)
@click.option("-o", "--dest", default="", type=click.STRING)
@click.option("-r", "--ratio", default=1.0, type=click.FloatRange(min=1e-3))
@fsave
@fopen
@fopen
@remove_filename
def scale(ratio, image_obj):
    """Scale image
    """
    new_w = int(ratio * image_obj.width)
    new_h = int(ratio * image_obj.height)
    image_obj.thumbnail((new_w, new_h), Image.ANTIALIAS)
    return image_obj


@click.command()
@click.option("-i", "--src", required=True, type=click.STRING)
@click.option("-o", "--dest", default="", type=click.STRING)
@click.option("-d", "--direction", default="vertical",
              type=click.Choice(["vertical", "horizontal"]))
@fsave
@fopen
@fopen
@remove_filename
def flip(direction, image_obj):
    """Flip image
    """
    _d = Image.FLIP_TOP_BOTTOM
    if direction == "vertical":
        _d = Image.FLIP_LEFT_RIGHT
    return image_obj.transpose(_d)


@click.command()
@click.option("-i", "--src", required=True, type=click.STRING)
@click.option("-o", "--dest", default="", type=click.STRING)
@click.option("-v", "--value", default=1, type=click.FloatRange(min=0, ))
@fsave
@fopen
@fopen
@remove_filename
def brightness(value, image_obj):
    """Control brightness color
    """
    enhancer = ImageEnhance.Brightness(image_obj)
    enhanced_im = enhancer.enhance(value)
    return enhanced_im


main.add_command(balance)
main.add_command(flip)
main.add_command(resize)
main.add_command(rotate)
main.add_command(scale)
main.add_command(show)
