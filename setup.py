#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Publish a new version:
$ git tag X.Y.Z -m "Release X.Y.Z"
$ git push --tags
$ pip install --upgrade twine wheel
$ python setup.py sdist bdist_wheel --universal
$ twine upload dist/*
"""

import codecs
import io
import re
from collections import OrderedDict
from setuptools import setup


IMTOOL_VERSION = '0.1.1'
IMTOOL_DOWNLOAD_URL = (
    'https://github.com/thanhngk/imtool/tarball/' + IMTOOL_VERSION
)


def read_file(filename):
    """
    Read a utf8 encoded text file and return its contents.
    """
    with codecs.open(filename, 'r', 'utf8') as f:
        return f.read()

setup(
    name="imtool",
    version="0.0.1",
    url="https://github.com/thanhngk/imtool",
    project_urls=OrderedDict((
        ("Documentation", "http://flask.pocoo.org/docs/"),
        ("Code", "https://github.com/pallets/flask"),
        ("Issue tracker", "https://github.com/pallets/flask/issues"),
    )),
    license="MIT",
    author="Nguyen Khac Thanh",
    author_email="nguyenkhacthanh244@gmail.com",
    maintainer="Nguyen Khac Thanh",
    maintainer_email="nguyenkhacthanh244@gmail.com",
    description="Image Manipulation tools",
    long_description=read_file("README.rst"),
    packages=["imtool"],
    include_package_data=True,
    zip_safe=False,
    platforms="any",
    python_requires=">=3.5",
    install_requires=[
        "pillow>=5.4.1",
        "click>=7.0",
    ],
    extras_require={
        "dotenv": ["python-dotenv"],
        "dev": [
            "pytest>=4.3.1",
            "pylint>=2.3.1",
            "tox>=3.8.0",
        ],
        "docs": [
            "sphinx",
            "pallets-sphinx-themes",
            "sphinxcontrib-log-cabinet",
        ]
    },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    entry_points={
        "console_scripts": [
            "imtool = imtool.cli:main",
        ],
    },
)
